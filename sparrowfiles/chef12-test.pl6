bash 'yum -y -q remove chef.x86_64';

module_run 'RemoteFile', %( 
    url       => 'https://packages.chef.io/files/stable/chef/12.17.44/el/6/chef-12.17.44-1.el6.x86_64.rpm',
    location  => '/tmp/chef-12.17.44-1.el6.x86_64.rpm'
);


bash "rpm -ivh /tmp/chef-12.17.44-1.el6.x86_64.rpm";

module_run 'Chef::Client', %(
  run-list => [
    "role[i360-dev]"
  ],
  log-level => 'info'
);
